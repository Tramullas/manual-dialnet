# Manual Dialnet

**Desarrollo de un manual de usuario para Dialnet**

Este repositorio contiene los documentos en reStructuredText creados durante el proceso de generación de un manual de usuario de Dialnet.

[Dialnet](https://dialnet.unirioja.es) es la principal fuente de información de publicaciones en español para ciencias humanas y sociales.

